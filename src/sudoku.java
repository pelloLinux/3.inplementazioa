import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class sudoku {

	private static int[][] taula; //hasieratik bukaerarainoko soluzioa hemen gordeko da
	private static int[][] taulaFinkoa; //hasieratik finkoak diren posizioak detektatzeko
	private static int taulaKop; //sudokuaren tamaina gordetzeko
	private static boolean segi; //Soluzio batera ailegatu garen edo ez jakiteko

	public static void main(String[] args) throws InterruptedException, FileNotFoundException {
		// TODO Auto-generated method stub

		long startTime;
		long endTime;

		//hasieraketak egin
		matrizeaEtaAldagaiakHasieratu();

		//hasierako matrizea inprimatu
		matrizeaInprimatu(taulaKop);

		//Hemen hasiko gara denbora kontatzen
		startTime = System.currentTimeMillis();

		//sudokua egingoduen funtzioari deitu
		backTrackSudoku(0, 0);

		System.out.println();
		System.out.println("**************************************************");

		//denbora kontatzea bukatu
		endTime = System.currentTimeMillis();

		if(!segi)
			matrizeaInprimatu(taulaKop);
		else 
			System.out.println("Sudokuak ez du soluziorik");

		System.out.println();
		System.out.println("Denbora: " + (endTime - startTime) + " milisegundu");


	}
	private static void backTrackSudoku(int i, int j) 
	{

		if(i==taulaKop)
			segi = false;//ona iritsi bagara SOLUZIOA aurkitu dugu
		
		//posizioa ez bada finkoa eta SOLUZIOA ez bada aurkitu
		else if(taulaFinkoa[i][j]==0 && segi)
		{
			for(int num=1; num <= taulaKop && segi; num++)
			{
				if(onartu(num, i, j))//posizio horretan onargarria den begiratu
				{
					//onartua izan denez, taulan idatzi
					taula[i][j]=num;
					if(j==taulaKop-1)
					{	//taularen eskuin aldean badago, errenkada bat jeitxi
						backTrackSudoku(i+1, 0);
					}
					else
					{	//ez dago taularen eskuin aldean, zutabea eskuineratu
						backTrackSudoku(i, j+1);
					}
				}
			}
		}
		else if(segi)//taulako zenbakia finkoa bada eta SOLUZIOA ez bada aurkitu
		{
			if(j==taulaKop-1)
			{	//taularen eskuin aldean badago, errenkada bat jeitxi
				backTrackSudoku(i+1, 0);
			}
			else
			{	//ez dago taularen eskuin aldean, zutabea eskuineratu
				backTrackSudoku(i, j+1);
			}
		}
	}

	private static boolean onartu(int num, int i, int  j)
	{
		//zutabean ba alago zenbaki hori(kima 1)
		for(int w=0; w<taulaKop; w++)
			if(w<i || taulaFinkoa[w][j]!=0)
				if(taula[w][j]==num)
					return false;

		//errenkadan ba aldago zenbaki hori(kima 2)
		for(int w=0; w<taulaKop; w++)
			if(w<j || taulaFinkoa[i][w]!=0)
				if(taula[i][w]==num)
					return false;


		//karratuan ba aldago zenbaki hori(kima 3)
		int kop = (int) Math.sqrt(taulaKop);

		int koadrante_i = kop*((int)i/kop);
		int koadrante_j = kop*((int)j/kop);

		boolean konprobatzeko = true;
		for(int w=koadrante_i; w<koadrante_i+kop; w++)
			for(int z=koadrante_j; z<koadrante_j+kop; z++)
			{
				if(w==i && z==j)konprobatzeko=false;
				else if(konprobatzeko || taulaFinkoa[w][z]!=0)
					if(taula[w][z]==num)
						return false;
			}

		//zenbakia posizio horretan onartua
		return true;
	}


	@SuppressWarnings("resource")
	private static void matrizeaEtaAldagaiakHasieratu() throws FileNotFoundException
	{
		Scanner s = new Scanner(System.in);
		System.out.println("Ze dimentsiotako sudokua?");
		taulaKop = s.nextInt();
		
		//hasieran soluzioa bilatu gabe dagoela hasieratu
		segi = true;
		
		//memoria alokatu
		taula = new int[taulaKop][taulaKop];
		taulaFinkoa = new int[taulaKop][taulaKop];
		//sudokuaren hasierako zenbakiak dituen fitxategia atzitu
		s = new Scanner(System.in);
		System.out.println("Fitxategiaren izena? ("+taulaKop+"x"+taulaKop+")");
		File file = new File("sudoku/"+s.nextLine());
		s = new Scanner(file);

		//taulako posizioak hasieratu
		for(int i=0; i<taulaKop; i++)
			for(int j=0; j<taulaKop; j++)
			{
				int num = s.nextInt();
				taula[i][j] = num;
				taulaFinkoa[i][j]=num;
			}
		s.close();
	}


	private static void matrizeaInprimatu(int kop)
	{
		boolean marrak = true;

		for(int i=0; i<kop; i++)
			for(int j=0; j<kop; j++)
			{
				if(j==kop-1)
				{
					if(taula[i][j]<10)
						System.out.println(" "+taula[i][j]);
					else
						System.out.println(taula[i][j]);
					marrak = true;
				}
				else if((i%Math.sqrt(kop)==0) && marrak)
				{
					for(int w=0; w<kop; w++)
						if(w==kop-1)
							System.out.println("  ");
						else System.out.print("  ");

					if(taula[i][j]<10)
						System.out.print(" "+taula[i][j]+" ");
					else
						System.out.print(taula[i][j]+" ");
					marrak = false;
				}
				else if(j%Math.sqrt(kop)==Math.sqrt(kop)-1)
				{
					if(taula[i][j]<10)
						System.out.print(" "+taula[i][j]);
					else
						System.out.print(taula[i][j]);
					System.out.print(" | ");
				}
				else{
					if(taula[i][j]<10)
						System.out.print(" "+taula[i][j]+" ");
					else
						System.out.print(taula[i][j]+" ");
				}
			}
	}
}
